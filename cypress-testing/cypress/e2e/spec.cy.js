/// <reference types="cypress" />

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImJlbm5ldHRsaXlhbnRvIiwiX2lkIjoiNjRhNTBiYTA3NzAzNzEwMDExNGU1MzFlIiwibmFtZSI6IkJlbm5ldHQiLCJ0ZWFjaGVyT3JnYW5pemF0aW9uIjoiIiwiaWF0IjoxNjg4NTM4MDE2LCJleHAiOjE2OTM3MjIwMTZ9.wwHE_YMuf-DARRBzOvBRjJqwUbsZ9i8x8KWgAMjU_-I'

describe ('Basic Test', () => {

  beforeEach(() => {

    cy.viewport(1280, 720)
    cy.visit('https://codedamn.com')

  })

  it('Basic elements exist', () => {
    cy.visit('https://codedamn.com')

    cy.contains('Learn coding with your personal AI').should('exist')

    cy.get('[data-testid=learn-popover-btn]').click()


  })

  it('Login page is good', () => {
    cy.contains('Login').click()
    cy.contains("Don't have an account?").should('exist')
    cy.contains("Sign in").should('exist')
    cy.contains("Or continue with").should('exist')
  })

  it('Login page links', () => {
    cy.contains('Login').click()
    cy.log('Testing Account creation')
    cy.contains('Create Account').click()
    cy.url().should('include', '/register')
    cy.go('back')
  })

  it('Typing with Cypress', () => {
    cy.contains('Login').click()

    cy.get('[data-testid=username]').type('admin')
    cy.get('[data-testid=password]').type('admin')
    cy.get('[data-testid=login]').click()

    cy.contains('Unable to authorize. Please check username/password combination').should('exist')
  })

  it('Working Login', () => {
    cy.contains('Login').click()

    cy.get('[data-testid=username]').type('bennett.liyanto@gmail.com')
    cy.get('[data-testid=password]').type('helloworld')
    cy.get('[data-testid=login]').click()

    cy.contains('Unable to authorize. Please check username/password combination').should('not.exist')
    cy.url().should('include', '/dashboard')
  })
})